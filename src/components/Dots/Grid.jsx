import React, { Component } from "react";
import Dot from "./Dot";

// require("../../assets/main.css");

export default class Grid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth,
      height: window.innerHeight,
      ratio: window.devicePixelRatio || 1
    };

    this.canvas = null;
    this.buffer = 8;
    this.rowCount = 0;
    this.colCount = 0;
    this.backgroundColor = "#fff";
    this.dots = [];
    this.dotRadius = 2;
    this.dotColor = "#bbb";
  }

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);

    this.canvas = document.getElementById("canvas");
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.initDots();
    this.draw();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight,
      ratio: window.devicePixelRatio || 1
    });

    if (this.canvas != null) {
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerHeight;
    }
  };

  componentDidUpdate() {
    this.updateDots();
    this.draw();
  }

  initDots() {
    const buffer = this.buffer;
    const rowCount = Math.ceil(this.state.height / buffer);
    const colCount = Math.ceil(this.state.width / buffer);

    var dots = [];

    for (var row = 0; row < rowCount; row++) {
      const y = row * buffer;

      dots[row] = [];

      for (var col = 0; col < colCount; col++) {
        const x = col * buffer;

        dots[row][col] = new Dot({
          visible: false,
          positionX: x,
          positionY: y,
          radius: this.dotRadius
        });
      }
    }

    this.dots = dots;
    this.rowCount = rowCount;
    this.colCount = colCount;
  }

  draw() {
    const context = this.canvas.getContext("2d");

    const canvasWidth = this.state.width;
    const canvasHeight = this.state.height;
    // context.clearRect();

    context.fillStyle = this.backgroundColor;
    context.fillRect(0, 0, canvasWidth, canvasHeight);

    for (var row = 0; row < this.rowCount; row++) {
      for (var col = 0; col < this.colCount; col++) {
        var dot = this.dots[row][col];
        var dotProperty = dot.props;

        if (dotProperty.visible === true) {
          context.beginPath();
          context.arc(
            dotProperty.positionX,
            dotProperty.positionY,
            dotProperty.radius,
            0,
            Math.PI * 2,
            false
          );

          //   context.fillStyle = this.backgroundColor;
          // } else {
          context.fillStyle = this.dotColor;
          // }
          context.fill();
          context.closePath();
        }
      }
    }
  }

  updateDots() {
    const dots = this.dots;
    const buffer = this.buffer;

    const mouseX = this.props.mouse.x;
    const mouseY = this.props.mouse.y;

    for (var row = 1; row < dots.length - 1; row++) {
      for (var col = 1; col < dots[row].length - 1; col++) {
        const dot = dots[row][col];
        const dotProperty = dot.props;
        const dotPositionX = dotProperty.positionX;
        const dotPositionY = dotProperty.positionY;

        const isWithinMouseRange =
          mouseX >= dotPositionX - buffer &&
          mouseX < dotPositionX + buffer &&
          mouseY >= dotPositionY - buffer &&
          mouseY < dotPositionY + buffer;

        const isAlreadyVisible = dotProperty.visible;
        const hasVisibleNeighbor =
          dots[row + 1][col + 1].props.visible ||
          dots[row + 1][col - 1].props.visible ||
          dots[row - 1][col + 1].props.visible ||
          dots[row - 1][col + 1].props.visible ||
          dots[row - 1][col - 1].props.visible;

        // isAlreadyVisible = durability
        // hasVisibleNeighbor = potency

        var weightedArray = this.createWeightedArray(isAlreadyVisible, 8)
          .concat(this.createWeightedArray(hasVisibleNeighbor, 8))
          .concat(this.createWeightedArray(false, 16));
        const randomIndex = Math.floor(Math.random() * weightedArray.length);

        if (isWithinMouseRange || weightedArray[randomIndex]) {
          dotProperty.visible = true;
        } else {
          dotProperty.visible = false;
        }
      }
    }
  }

  createWeightedArray(item, weight) {
    var array = [];

    for (var i = 0; i < weight; i++) {
      array[i] = item;
    }

    return array;
  }

  render() {
    return (
      <div id="dotGrid">
        <canvas id="canvas" className="canvas" ref="canvas" />
      </div>
    );
  }
}