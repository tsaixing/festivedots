import React, { Component } from "react";
import Page from "../Page";

export default class Error404 extends Component {
  render() {
    return (
      <Page>
        <h1>Page not found!</h1>
      </Page>
    );
  }
}
