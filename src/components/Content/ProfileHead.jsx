import React, { Component } from "react";

import PortfolioLinks from "./PortfolioLinks";

export default class ProfileHead extends Component {
  render() {
    const iconSize = 25;

    return (
      <div id="profileHead">
        <h1>Tsai-Xing Wong</h1>
        <h4>
          <span className="address">
            <img
              src={require("../../assets/email.png")}
              alt="email"
              height={iconSize}
            />{" "}
            hello@tsaixing.com
          </span>
        </h4>

        <h4>
          <a
            href="https://linkedin.com/in/tsaixing"
            target="_blank"
            rel="noopener noreferrer"
          >
            <span className="address">
              <img
                src={require("../../assets/linkedin.png")}
                alt="Linkedin"
                height={iconSize}
              />
              in/tsaixing
            </span>
          </a>
        </h4>

        <h4>
          <a
            href="https://gitlab.com/tsaixing"
            target="_blank"
            rel="noopener noreferrer"
          >
            <span className="address">
              <img
                src={require("../../assets/gitlab.png")}
                alt="Gitlab"
                height={iconSize}
              />
              /tsaixing
            </span>
          </a>
        </h4>

        <PortfolioLinks />
      </div>
    );
  }
}
