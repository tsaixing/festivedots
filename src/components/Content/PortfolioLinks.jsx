import React, { Component } from "react";

export default class PortfolioLinks extends Component {
  render() {

    return (
      <div id="portfolio">
        <ul>
          <li>
          
          <a
            href="http://adyst.tumblr.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src={require("../../assets/art.png")}
              alt="Art"
            />
            </a>
          </li>
          <li>
          
          <a
            href="https://tsaixing.gitlab.io/lostinspace"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src={require("../../assets/lostinspace.svg")}
              alt="Space"
            />
            </a>
          </li>
        </ul>
      </div>
    );
  }
}
