import React, { Component } from "react";

import ProfileHead from "./components/Content/ProfileHead";
import Grid from "./components/Dots/Grid";

require("./assets/main.css");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mouse: {
        x: 0,
        y: 0
      }
    };
    this.fps = 10;
  }

  componentDidMount() {
    document.title = "Tsai-Xing Wong";

    if (this.isMobile()) {
      this.setInterval();
    }
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  setInterval() {
    this.intervalID = setInterval(() => {
      this.setState({
        mouse: {
          x: this.randomizeSeed(0, window.innerWidth),
          y: this.randomizeSeed(0, window.innerHeight)
        }
      });
    }, 1000 / this.fps);
  }

  _onMouseMove(e) {
    this.setState({
      mouse: {
        x: e.clientX,
        y: e.clientY
      }
    });
  }

  isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini|Mobile/i.test(
      navigator.userAgent
    );
  }

  randomizeSeed(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  render() {
    return (
      <div
        id="app"
        onMouseMove={this._onMouseMove.bind(this)}
        onMouseDown={this._onMouseMove.bind(this)}
      >
        <ProfileHead mouse={this.state.mouse} />
        <Grid mouse={this.state.mouse} />
      </div>
    );
  }
}

export default App;
